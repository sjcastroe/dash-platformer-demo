# **Dash Platformer Demo** #

## **About** ##
Dash Platformer Demo (or Flag Collector Simulator as it was originally called) is a short first person 3D platformer created at Cal Poly Pomona's annual hackathon, HackPoly 2015 ([Project Page](http://challengepost.com/software/flag-collection-simulator)). The demo was created with Unity in 18 hours by a team of four developers with no prior experience using the game engine.

## **Concept** ##
The idea was to create a 3D platformer that was easy to pick up and understand but difficult to master. The game is fleshed out by the level design which requires the player to use the simple mechanics in creative ways. The two main mechanics are double jumping[1] and dashing[2] which require precise timing and a feel for physical factors like inertia when moving on moving platforms. These two game mechanics should be mixed and matched to and used with power ups found within the level that enhance your abilities.

![dash2.gif](https://bitbucket.org/repo/XKaKAo/images/417672790-dash2.gif)[1]
![dash1.gif](https://bitbucket.org/repo/XKaKAo/images/1637767700-dash1.gif)[2]


