﻿using UnityEngine;
using System.Collections;

public class movePlatform : MonoBehaviour {
	public Vector3 target1 = new Vector3 (0, 0, 0);
	public Vector3 target2 = new Vector3 (0, 0, 0);
	public Vector3 position = new Vector3 (0, 0, 0);
	public float moveSpeed = 1.0f;
	int xComparison = 0;
	int yComparison = 0;
	int zComparison = 0;
	
	// Use this for initialization
	void Start () {
		if (target1.x > target2.x) {
			xComparison = 1;
			print ("x Comparison set to 1");
		}
		else if(target1.x < target2.x) {
			xComparison = 2;
			print ("x Comparison set to 2");
		}
		if (target1.y > target2.y) {
			yComparison = 1;
			print ("y Comparison set to 1");
		}
		else if(target1.y < target2.y) {
			yComparison = 2;
			print ("y Comparison set to 2");
		}
		if (target1.z > target2.z) {
			zComparison = 1;
			print("z Comparison set to 1");
		}
		else if(target1.z < target2.z){
			zComparison = 2;
			print ("z Comparison set to 2");
		}
		this.gameObject.transform.position = target1;
	}
	
	// Update is called once per frame
	void Update () {
		position = this.gameObject.transform.position;
		switch (xComparison) {
		case 1:
			if (position.x >= target1.x)
				moveToPoint (target2);
			if (position.x <= target2.x)
				moveToPoint (target1);
			break;
		case 2:
			if (position.x >= target2.x)
				moveToPoint (target1);
			if (position.x <= target1.x)
				moveToPoint (target2);
			break;
		}

		switch (yComparison) {
		case 1:
			if (position.y >= target1.y)
				moveToPoint (target2);
			if (position.y <= target2.y)
				moveToPoint (target1);
			break;

		case 2:
			if (position.y >= target2.y)
				moveToPoint (target1);
			if (position.y <= target1.y)
				moveToPoint (target2);
			break;
		}

		switch (zComparison) {
		case 1:
			if (position.z >= target1.z)
				moveToPoint (target2);
			if (position.z <= target2.z)
				moveToPoint (target1);
			break;
		case 2:
			if (position.z >= target2.z)
				moveToPoint (target1);
			if (position.z <= target1.z)
				moveToPoint (target2);
			break;
		}

	}

	void moveToPoint(Vector3 point){
		Vector3 distance = (point - position);
		rigidbody.velocity = distance/moveSpeed;
		
	}

}
