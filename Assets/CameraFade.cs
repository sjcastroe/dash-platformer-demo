﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class CameraFade : MonoBehaviour
{
	public bool checkpointText = false;
	public Vector3 checkpoint = new Vector3(3, 5, 3);
	public static Texture2D Fade;
	public bool fadingOut = false;
	public float alphaFadeValue = 0;
	public float fadeSpeed = 0.2f;
	public Color fadingColor = Color.red;
	private static CameraFade _current;
	private static bool initialized;
	public static CameraFade Current
	{
		get
		{
			if(_current == null && !initialized)
			{
				initialized = true;
				var go = new GameObject("CameraFade");
				_current = go.AddComponent<CameraFade>();
			}
			return _current;
		}
		set
		{
			_current = value;
		}
	}
	
	void OnDestroy()
	{
		if(Current == this)
		{
			initialized = false;
			Current = null;
		}
	}
	
	
	void Start()
	{
		if(_current == null )
		{
			_current = this;
		}
		else if(_current != this)
		{
			Destroy(this);
		}
		if (Fade == null)
		{
			Fade = new Texture2D(1, 1);
			Fade.SetPixel(0, 0, new Color(1, 1, 1, 1));
			
		}
	}
	
	
	void Update()
	{
		alphaFadeValue = Mathf.Clamp01(alphaFadeValue + ((Time.deltaTime / fadeSpeed) * (fadingOut ? 1 : -1)));
		useGUILayout = alphaFadeValue != 0;
		if (alphaFadeValue != 0)
		{
			fadingColor.a = alphaFadeValue;
			Fade.SetPixel(0, 0, fadingColor);
			Fade.Apply();
		}

		if(transform.position.y < 4) {
			CameraFade.Current.fadingOut = true;
			if(transform.position.y < 2){
				transform.position = checkpoint;
				alphaFadeValue = 0;
			}
		}
		else
			CameraFade.Current.fadingOut = false;
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag=="fireball")
		{
			CameraFade.Current.fadingOut = true;
			//CameraFade.Current.fadeSpeed = 1;
			transform.position = checkpoint;
			alphaFadeValue = 255;
			CameraFade.Current.fadeSpeed = 0.2f;
		}
	}
	void OnGUI()
	{
		if(alphaFadeValue != 0 && Event.current.type==EventType.repaint)
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), Fade);
		if (checkpointText) {
			GUI.Label(new Rect(0, 0, (Screen.width/2),Screen.height), "Checkpoint Achieved"); 
			Invoke("checkpointTextFalse", 3.0f);
		}
	}

	void checkpointTextFalse(){
		checkpointText = false;
	}
}